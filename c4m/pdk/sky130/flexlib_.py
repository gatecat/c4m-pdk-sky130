# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
from c4m.flexcell.library import Library

from .pdkmaster import tech, cktfab, layoutfab

__all__ = ["flexlib"]

prims = tech.primitives
flexlib = Library(lambda_=0.06).convert2pdkmaster("FlexLib",
    tech=tech, cktfab=cktfab, layoutfab=layoutfab,
    nmos=prims.nfet_01v8, pmos=prims.pfet_01v8,
    nimplant=prims.nsdm, pimplant=prims.psdm,
)
